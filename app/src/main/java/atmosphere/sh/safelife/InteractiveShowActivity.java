package atmosphere.sh.safelife;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class InteractiveShowActivity extends AppCompatActivity
{
    CountDownTimer countDownTimer,countDownTimer2;
    int counter;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interactive_show);

        progressBar = findViewById(R.id.progress_bar);

        progressBar.setVisibility(View.INVISIBLE);

        countDownTimer = new CountDownTimer(164000, 1)
        {
            public void onTick(long millisUntilFinished)
            {

            }
            public  void onFinish()
            {
                countDownTimer2 = new CountDownTimer(5000,1000)
                {
                    @Override
                    public void onTick(long millisUntilFinished)
                    {
                        progressBar.setVisibility(View.VISIBLE);

                        progressBar.setProgress(counter);
                        counter = counter + 20;
                    }

                    @Override
                    public void onFinish()
                    {
                        progressBar.setProgress(100);
                        Toast.makeText(getApplicationContext(), "Finished ...", Toast.LENGTH_SHORT).show();
                    }
                }.start();
            }
        }.start();
    }
}
