package atmosphere.sh.safelife;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity
{
    VideoView videoView;
    CardView cardView;

    String url1 = "https://firebasestorage.googleapis.com/v0/b/safelife-edad2.appspot.com/o/Videos%2Ffff.mp4?alt=media&token=10948951-478f-46bd-b6e0-29ac2cb387f8";
    String url2 = "https://firebasestorage.googleapis.com/v0/b/safelife-edad2.appspot.com/o/Videos%2Fdirtyfinal.mp4?alt=media&token=3eb56e1b-fbfd-4fbd-a1ee-0d087ab4b0a8";

    Dialog dialog;

    TextView timer_txt;
    Button yes_btn,no_btn;

    CountDownTimer countDownTimer;
    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        videoView = findViewById(R.id.video_view);
        cardView = findViewById(R.id.question_card);
        timer_txt = findViewById(R.id.timer_txt);
        yes_btn = findViewById(R.id.yes_btn);
        no_btn = findViewById(R.id.no_btn);

        cardView.setVisibility(View.GONE);

        loadingDialog();

        Uri uri = Uri.parse(url1);

        videoView.setVideoURI(uri);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mp)
            {
                dialog.dismiss();
                videoView.requestFocus();
                videoView.start();
            }
        });

        videoView.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                cardView.setVisibility(View.VISIBLE);
                cardView.setScaleX(0);
                cardView.setScaleY(0);

                cardView.animate().scaleY(1).scaleX(1).setDuration(1000);

                videoView.pause();

                yes_btn.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        loadingDialog();

                        Uri uri = Uri.parse(url2);

                        videoView.setVideoURI(uri);
                        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                        {
                            @Override
                            public void onPrepared(MediaPlayer mp)
                            {
                                dialog.dismiss();
                                videoView.requestFocus();
                                videoView.start();
                            }
                        });
                        countDownTimer.cancel();
                        cardView.setVisibility(View.GONE);
                    }
                });

                no_btn.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        videoView.start();
                        countDownTimer.cancel();

                        cardView.setVisibility(View.GONE);
                    }
                });

                countDownTimer = new CountDownTimer(4000, 1000)
                {
                    public void onTick(long millisUntilFinished)
                    {
                        timer_txt.setText(String.valueOf(counter));
                        counter++;
                    }
                    public  void onFinish()
                    {
                        videoView.start();

                        cardView.setVisibility(View.GONE);
                    }
                }.start();
            }
        }, 164000);

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                onBackPressed();
            }
        });
    }

    private void loadingDialog()
    {
        dialog = new Dialog(VideoActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.loading_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes();
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
