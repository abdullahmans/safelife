package atmosphere.sh.safelife;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Timer;
import java.util.TimerTask;

import atmosphere.sh.safelife.Models.UserModel;

public class SplashScreenActivity extends AppCompatActivity
{
    String name;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null)
        {
            TimerTask task = new TimerTask()
            {
                @Override
                public void run()
                {
                    String name = user.getDisplayName();

                    if (name.length() == 0)
                    {
                        returnName(user.getUid());
                    } else
                        {
                            // go to the main activity
                            Intent i = new Intent(getApplicationContext(), StartActivity.class);
                            i.putExtra("name",name);
                            startActivity(i);
                            // kill current activity
                            finish();
                        }
                }
            };
            // Show splash screen for 3 seconds
            new Timer().schedule(task, 3000);
        } else
        {
            TimerTask task = new TimerTask() {
                @Override
                public void run()
                {
                    // go to the main activity
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    // kill current activity
                    finish();
                }
            };
            // Show splash screen for 3 seconds
            new Timer().schedule(task, 3000);
        }
    }

    public void returnName (String uid)
    {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("Users").child(uid).addListenerForSingleValueEvent(
                new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        // Get user value
                        UserModel userModel = dataSnapshot.getValue(UserModel.class);

                        name = userModel.getUsername();

                        updateUI(name);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {
                        Toast.makeText(getApplicationContext(), "can\'t fetch data", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void updateUI (String name)
    {
        Intent intent = new Intent(getApplicationContext(), StartActivity.class);
        intent.putExtra("name", name);
        startActivity(intent);
    }
}
